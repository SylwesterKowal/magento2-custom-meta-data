<?php

namespace M21\CustomMeta\Observer;

class Afterproductload implements \Magento\Framework\Event\ObserverInterface
{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getProduct();
        $manufacturer = $product->getAttributeText('manufacturer');
        $metaDescription = $manufacturer . ' ➤ ' . $product->getName() . ' ➤ ' . $product->getManufacturerCode() . ' ➤ ' . $product->getSku();
        $product->setMetaDescription($metaDescription);
        $product->setIsMetaApplied(true);

        return $this;
    }
}